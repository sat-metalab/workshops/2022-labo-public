Labo Public - MATERIA
=====================

Ce dépôt est destiné à contenir l'ensemble des fichiers de documentation, configuration, etc. qui seront créés et mis en œuvre dans le cadre de l'atelier [Labo Public](https://www.elektramontreal.ca/labo-public-call-participation?lang=fr) organisé par Hexagram et Elektra à Montréal, début septembre 2022.

